Feito por: Dirlene Preilipper / Academia Java Proway-Capgemini

Questão escolhida: Número 5 - Complexidade 4


8. Na sua interpretação:
a. Qual foi o nível de dificuldade da implementação da questão do bloco A?
[ ] Muito Fácil
[ ] Fácil
[ ] Médio
[X] Difícil
[ ] Muito Difícil

b. Qual foi o nível de dificuldade para encontrar os erros do programa da questão 7?
[ ] Muito Fácil
[X] Fácil
[ ] Médio
[ ] Difícil
[ ] Muito Difícil