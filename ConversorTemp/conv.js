
var btnConverter = document.getElementById("btnConverter")

btnConverter.onclick = function() {
    let tempEntrada = Number(document.getElementById("idTemp").value)
    let unidadeEntrada = document.querySelector("#idUnidadeOrigem").value
    let unidadeConversao = document.querySelector("#idUnidadeConvertido").value

    //Conversao da unidade de entrada para Celsius 
    let tempCelsius = 0;

    switch (unidadeEntrada) {
        case "C":
            tempCelsius = tempEntrada
            break;
        case "F":
            tempCelsius = (tempEntrada * 5 - 160) / 9
            break;
        case "K":
            tempCelsius = tempEntrada - 273
            break;
        default:
            break;
    }

    //Conversao de celsius para a unidade de saida 
    let tempConvertido = 0;

    switch (unidadeConversao) {
        case "C":
            tempConvertido = tempCelsius
            break;
        case "F":
            tempConvertido = (9 * tempCelsius + 160) / 5
            break;
        case "K":
            tempConvertido = tempCelsius + 273
            break;
        default:
            break;
    }

    document.getElementById("idTempConvertido").value = tempConvertido


}
